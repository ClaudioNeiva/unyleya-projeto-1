# CIB - Calculadora de Impostos B3

Objetivo do projeto: Calcular o valor do imposto de renda para operações na bolsa de valores B3 e detalhar a memória de cálculo e emitir correspondente DARF. Além de apoiar o preenchimento da declaração de ajuste a anual de imposto de renda no tocante à ficha de renda variável.

## Time Scrum

- Antonio Claudio Neiva - Scrum Master;
- João da Silva - Product Owner;
- Maria José Gomes - Desenvolvedora;
- Joaquim Silveira - Desenvolvedor;
- Carlos Jorge Amado - Desenvolvedor;
- Ana Silva - Desenvolvedora;
- Mateus Carvalho - Desenvolvedor;
- Marcelo Augusto - Desenvolvedor;

## Responsabilidades por papel

- Scrum Master – Garantir que as práticas Ágeis serão seguidas e remover impedimentos;
- Product Owner – Maximizar o valor do produto e manter o backlog do mesmo. Fazer a interface com as partes interessadas, servido como referencial para o time de desenvolvimento em suas necessidades de definições;
- Desenvolvedor – Quebrar as histórias em tarefes, estimar as mesmas, gerenciar como o trabalho de desenvolvimento será realizado, projetar, implementar, testar e o que mais for necessário para produzir o software;

## Backlog

- Histórias de usuário podem ser consultadas em Issues -> Lista;
- Scrum board pode ser consultado em Issues -> Painéis (a raia Open contém o Product Backlog);
- Critérios de aceite estão dentro das Issues.

## Sprints e releases

- As sprints e releases podem ser consultadas em Issues -> Marcos
